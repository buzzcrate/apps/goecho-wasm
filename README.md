# GoEcho
[[_TOC_]]

# Summary

A very simple program to deploy and demostrate a simple pipeline that compiles a binary. The GoEcho example simply echo's back after the the `/` int he URL anything written. *NOTE:* If testing with curl, spaces must be represented with `%20`.

# Kubernetes Deployment

K3s will deploy the container with `kubectl apply -f goecho-deploy.yaml`. 

# Pipeline

The GitLab pipeline uses shell runners and uses buildah on a Arch Linux hosts. The pipeline is set to build only on the master branch.
